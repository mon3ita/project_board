from django.contrib.auth.models import User
from .models import UserProfile

import graphene
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType

from graphql_jwt.decorators import login_required


class UserType(DjangoObjectType):
    class Meta:
        model = User


class UserProfileType(DjangoObjectType):
    class Meta:
        model = UserProfile


class UserInputType(graphene.InputObjectType):
    id = graphene.ID()
    username = graphene.String()


class UserRegistration(graphene.Mutation):
    class Arguments:
        username = graphene.String()
        email = graphene.String()
        password = graphene.String()

    user = graphene.Field(UserType)
    error = graphene.String()

    def mutate(self, info, username, email, password):
        user = User.objects.create_user(
            username=username, email=email, password=password
        )
        return UserRegistration(user=user)


class UserProfileUpdate(graphene.Mutation):
    class Arguments:
        pk = graphene.ID()
        first_name = graphene.String()
        last_name = graphene.String()
        password = graphene.String()
        about_me = graphene.String()
        image = graphene.String()

    error = graphene.String()
    success = graphene.String()

    @login_required
    def mutate(self, info, **kwargs):
        user_profile = UserProfile.objects.filter(pk=info.context.user.id)

        if not user_profile:
            return UserProfileUpdate(error="User not found.")

        # print(info.context.FILE['avatar'])
        if set(["password", "first_name", "last_name"]).intersection(kwargs.keys()):
            user = user_profile.get().user
            user.first_name = kwargs.get("first_name", "")
            user.last_name = kwargs.get("last_name", "")

            try:
                del kwargs["first_name"]
            except:
                pass

            try:
                del kwargs["last_name"]
            except:
                pass

            try:
                del kwargs["password"]
            except:
                pass
            user.save()
            # password

        file = info.context.FILES["avatar"]
        if file:
            print(file)
            user_profile.update(avatar=file)

        user_profile.update(
            about_me=kwargs.get("about_me", ""),
            description=kwargs.get("description", ""),
        )

        return UserProfileUpdate(success="User was updated successfully.")


class Query(object):
    user_by_name = graphene.Field(UserType, username=graphene.String())
    user_profile = graphene.Field(UserProfileType)
    users = graphene.List(UserType)

    def resolve_users(self, info, **kwargs):
        return User.objects.all()

    def resolve_user_by_name(self, info, username):
        return User.objects.get(username=username)


class Mutation(graphene.ObjectType):
    registration = UserRegistration.Field()
    update_profile = UserProfileUpdate.Field()
