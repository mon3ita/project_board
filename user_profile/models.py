from django.db import models

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from django.utils.translation import gettext_lazy as _

from project.models import Project, Task


class UserProfile(models.Model):
    user = models.OneToOneField(
        User, verbose_name="user", related_name="profile", on_delete=models.CASCADE
    )
    about_me = models.TextField(blank=True)
    avatar = models.ImageField(
        upload_to="./static", default="./static/default/default.png"
    )

    def __str__(self):
        return f"{self.user.username}'s profile"


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwaegs):
    instance.profile.save()
