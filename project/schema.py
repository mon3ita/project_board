from .models import Project, Task, Category

import graphene
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType

from graphql_jwt.decorators import login_required

from datetime import datetime


class CategoryType(DjangoObjectType):
    class Meta:
        model = Category


class ProjectType(DjangoObjectType):
    id = graphene.ID(source="pk", required=True)

    class Meta:
        model = Project


class TaskType(DjangoObjectType):
    class Meta:
        model = Task


class ProjectCreate(graphene.Mutation):
    class Arguments:
        name = graphene.String()
        description = graphene.String()
        category = graphene.String()

    project = graphene.Field(ProjectType)

    @login_required
    def mutate(self, info, name, description, category):
        project = Project.objects.create(
            name=name,
            description=description,
            category=category,
            owner_id=info.context.user.id,
        )
        return ProjectCreate(project=project)


class ProjectUpdate(graphene.Mutation):
    class Arguments:
        pk = graphene.ID()
        name = graphene.String()
        description = graphene.String()
        deadline = graphene.DateTime()

    project = graphene.Field(ProjectType)
    error = graphene.String()

    @login_required
    def mutate(self, info, pk, **kwargs):
        project = Project.objects.filter(pk=pk, owner_id=info.context.user.id)

        if not project:
            return ProjectUpdate(error="Project doesn't exist.")

        project.update(**kwargs)
        return ProjectUpdate(project=project)


class ProjectDelete(graphene.Mutation):
    class Arguments:
        pk = graphene.ID()

    error = graphene.String()
    success = graphene.String()

    @login_required
    def mutate(self, info, pk, **kwargs):
        project = Project.objects.filter(pk=pk, owner_id=info.context.user.id)

        if not project:
            return ProjectDelete(error="Project doesn't exist.")

        return ProjectDelete(success="Project was deleted successfully.")


class TaskCreate(graphene.Mutation):
    class Arguments:
        project_id = graphene.ID()
        name = graphene.String()
        description = graphene.String()

    task = graphene.Field(TaskType)

    @login_required
    def mutate(self, info, project_id, name, description):
        task = Task.objects.create(
            project_id=project_id,
            name=name,
            description=description,
            owner_id=info.context.user.id,
        )
        return TaskCreate(task=task)


class TaskUpdate(graphene.Mutation):
    class Arguments:
        pk = graphene.ID()
        name = graphene.String()
        deadline = graphene.String()
        done = graphene.Boolean()
        description = graphene.String()

    task = graphene.Field(TaskType)
    error = graphene.String()

    @login_required
    def mutate(self, info, pk, **kwargs):
        task = Task.objects.filter(pk=pk, owner_id=info.context.user.id)

        if not task:
            return TaskUpdate(error="Task not found.")

        try:
            kwargs["deadline"] = datetime.strptime(
                kwargs["deadline"], "%Y-%m-%d %H:%M:%S"
            )
        except:
            pass
        task.update(**kwargs)

        return TaskUpdate(task=task.get())


class TaskDelete(graphene.Mutation):
    class Arguments:
        pk = graphene.ID()

    error = graphene.String()
    success = graphene.String()

    @login_required
    def mutate(self, info, pk, name, description):
        task = Task.objects.filter(pk=pk, owner_id=info.context.user.id)

        if not task:
            return TaskDelete(error="Task not found.")

        task.delete()
        return TaskDelete(success="Task was deleted successfully.")


class Query(object):
    project_by_id = graphene.Field(ProjectType, pk=graphene.ID())
    task_by_id = graphene.Field(TaskType, pk=graphene.ID())

    projects = graphene.List(ProjectType)
    tasks = graphene.List(TaskType)
    categories = graphene.List(CategoryType)

    def resolve_project_by_id(self, info, pk):
        return Project.objects.get(pk=pk)

    def resolve_task_by_id(self, info, pk):
        return Task.objects.get(pk=pk)

    def resolve_projects(self, info, **kwargs):
        return Project.objects.all()

    def resolve_tasks(self, info, **kwargs):
        return Task.objects.all()

    def resolve_categories(self, info, **kwargs):
        return Category.objects.all()


class Mutation(graphene.ObjectType):
    project_create = ProjectCreate.Field()
    project_update = ProjectUpdate.Field()
    project_delete = ProjectDelete.Field()

    task_create = TaskCreate.Field()
    task_update = TaskUpdate.Field()
    task_delete = TaskDelete.Field()
