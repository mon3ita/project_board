from django.db import models
from django.utils.translation import gettext_lazy as _

from datetime import datetime, timedelta

from django.contrib.auth.models import User


def default_datetime():
    return datetime.now() + timedelta(days=3000)


class Category(models.Model):
    name = models.CharField(max_length=30, unique=True)

    class Meta:
        verbose_name_plural = "categories"

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField()
    done = models.BooleanField(default=False)
    private = models.BooleanField(default=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    deadline = models.DateTimeField(default=default_datetime, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    category = models.CharField(max_length=30, default="Undefined")

    def __str__(self):
        return self.name


class Task(models.Model):
    name = models.CharField(max_length=150)
    description = models.TextField(blank=True)

    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    deadline = models.DateTimeField(default=default_datetime, blank=True)
    done = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
