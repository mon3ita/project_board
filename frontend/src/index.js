import { hot } from "react-hot-loader/root";

import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import reportWebVitals from './reportWebVitals';

import { BrowserRouter } from 'react-router-dom';

const render = (Component) =>
  ReactDOM.render(
  <BrowserRouter>
    <Component />
  </BrowserRouter>, 
document.getElementById("root"));

render(hot(App));

reportWebVitals();
