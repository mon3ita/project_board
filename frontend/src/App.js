
import Header from "./components/layout/Header";
import Body from "./components/layout/Body";

function App() {
  return (
    <div>
      <Header />
      <Body />
    </div>
  );
}

export default App;
