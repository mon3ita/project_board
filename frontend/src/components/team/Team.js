import React, { Component } from "react";

import { Link } from "react-router-dom";

export default class Team extends Component {

    constructor(props) {
        super(props);

        this.state = {
            team: this.props.team
        };
    }

    render() {
        const team = this.state.team.map((member, idx) =>
            <span><Link to={`/users/${member.username}`}>{member.username}</Link>,&nbsp;</span>
        );

        return(
            <div className="row">
                Team:&nbsp;&nbsp;{team}
            </div>
        )
    }
}