import React, { Component } from "react";

import { axios_instance, getUser } from "../../utils";

export default class TeamCreateForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            users: [],
            team: [],
            type: ""
        };

        this.handleChangeTeam = this.handleChangeTeam.bind(this);
        this.handleCreateTeam = this.handleCreateTeam.bidn(this);
    }

    componentDidMount() {
        //check type project or task
        const token = getUser()[0];
        const headers = {
            'Authorization': `JWT ${token}`
        };

        const query = `
            query {
                users {
                    pk
                    username
                }
            }
        `;

        axios_instance.post("/", {
            query: query
        }, {
            headers: headers
        }).then(response => {
            this.setState({ users: response.data.data.users });
        }).catch(err => {
            console.log(err);
        });
    }

    handleChangeTeam(e) {
        //fix
        this.setState({ team: e.target.value });
    }

    handleCreateTeam(e) {

        const token = getUser()[0];
        const headers = {
            'Authorization': `JWT ${token}`
        };

        const team = this.state.team;

        const type = String.prototype.toUpperCase(this.state.type);
        const type_id = `${this.state.type}_id`;
        const _id = this.props.match.params.task_id || this.props.match.params.id;
        const mutate = `
            mutate {
                team${type}Create(`${type_id}`: `${_id}`, users: "`${team}`") {
                    pk
                    team
                }
            }
        `;

        axios_instance.post("/", {
            query: mutate
        }, {
            headers: headers
        }).then(response => {

        }).catch(err => {
            console.log(err);
        })

        e.preventDefault();
    }

    render() {
        const users = this.state.users.map((user, idx) =>
            <option key={idx}>{user.username}</option>
        );

        return(
            <form className="form">
                <div className="form-group">
                    <select className="form-select" onChange={(e) => this.handleChangeTeam(e) } multiple>
                        {options}
                    </select>
                </div>

                <div className="form-group">
                    <button className="btn btn-success" onClick={(e) => this.handleCreateTeam(e) } >Create</button>
                </div>
            </form>
        )
    }
}