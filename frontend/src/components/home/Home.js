import React, { Component } from "react";

import { Link } from "react-router-dom";

import { getUser } from "../../utils";

export default class Home extends Component {

    render() {
        const user = getUser()[0] || "Guest";

        return(
            <div>
                <h1>
                    Welcome to ProjectBoard!
                </h1>
                {user != "Guest" ? "" :
                    <div>
                        <i>Start your journey by&nbsp;&nbsp;&nbsp;<Link to="/signin" className="btn btn-primary btn-sm">Sign In</Link>&nbsp;&nbsp;&nbsp;&nbsp;or&nbsp;&nbsp;
                           <Link to="/signup" className="btn btn-primary btn-sm">Sign Up</Link>
                        </i>
                    </div>
                }
            </div>
        )
    }
}