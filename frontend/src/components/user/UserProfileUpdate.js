import React, { Component } from "react";

import { axios_instance, getUser, limit_text } from "../../utils";

export default class UserProfileUpdate extends Component {

    constructor(props) {
        super(props);

        this.state = {
            user: {},
            profile: {},
            password: "",
            newPassword: "",
            errors: []
        };

        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleAboutMeChange = this.handleAboutMeChange.bind(this);
        this.handleAvatarChange = this.handleAvatarChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleNewPasswordChange = this.handleNewPasswordChange.bind(this);

        this.handleUpdateProfile = this.handleUpdateProfile.bind(this);
        this.handleUpdatePassword = this.handleUpdatePassword.bind(this);
    }

    componentDidMount() {
        const username = this.props.match.params.username;

        const query = `query {
            userByName(username:"${username}") {
                id
                username
                firstName
                lastName
                email
                profile {
                    aboutMe
                    avatar
                }
            }
        }`;

        axios_instance.post("/", {
            query: query
        }).then(response => {
            this.setState({profile: response.data.data.userByName.profile,
                            user: response.data.data.userByName});
        }).catch(err => {
            console.log(err);
        });
    }

    handleFirstNameChange(e) {
        const user = this.state.user;
        user.firstName = e.target.value;
        this.setState({ user });
    }

    handleLastNameChange(e) {
        const user = this.state.user;
        user.lastName = e.target.value;
        this.setState({ user });
    }

    handleEmailChange(e) {
        const user = this.state.user;
        user.email = e.target.value;
        this.setState({ user });
    }

    handlePasswordChange(e) {
        this.setState({ password: e.target.value });
    }

    handleNewPasswordChange(e) {
        this.setState({ newPassword: e.target.value });
    }

    handleAvatarChange(e) {
        const profile = this.state.profile;
        profile.avatar = e.target.files[0];
        this.setState({ profile });
    }

    handleAboutMeChange(e) {
        const profile = this.state.profile;
        profile.aboutMe = e.target.value;
        this.setState({ profile });
    }

    handleUpdateProfile(e) {
        const { user, profile } = this.state;

        //FIX
        const data = new FormData();
        if(profile.avatar != './static/default/default.png')
            data.append('avatar', profile.avatar);

        const mutation = `
            mutation updateUser($aboutMe: String!, $firstName: String!, $lastName: String!) {
                updateProfile(aboutMe: $aboutMe,
                            firstName: $firstName,
                            lastName: $lastName) {
                        success
                }
            }
        `;

        const variables = {
            aboutMe: profile.aboutMe,
            firstName: user.firstName,
            lastName: user.lastName
        };

        const token = getUser()[0];
        const headers = {
            "Authorization": `JWT ${token}`,
        };

        const errors = [];
        axios_instance.post("/", {
            query: query,
            variables: variables
        },  {
            headers: headers,
        }).then(response => {
            console.log(response)
            if(response.data.data.errors) {
                for(let error of response.data.data.errors) {
                    errors.push(error.message);
                }
                this.setState({ errors });
            } else {
                window.location.href = `/users/${user.username}`;
            }
        }).catch(err => console.log(err));

        e.preventDefault();
    }

    handleUpdatePassword(e) {

        e.preventDefault();
    }

    render() {
        const { user, profile } = this.state;

        const errors = this.state.errors.map((err, idx) =>
            <div className="alert alert-danger">
                {err}
            </div>
        );

        return(
            <form className="form">
                <h1>{user.username}'s profile</h1>
                <hr/>
                {this.state.errors.length ? <div>{errors}<hr/></div> : ""}
                <h2>User Profile Settings</h2>
                <hr/>
                <div className="form-group">
                    <input type="text" className="form-control" value={user.firstName} onChange={(e) => this.handleFirstNameChange(e) } placeholder="First name" />
                </div>
                <div className="form-group">
                    <input type="text" className="form-control" value={user.lasttName} onChange={(e) => this.handleLastNameChange(e) } placeholder="Last name" />
                </div>

                <div className="form-group">
                    <textarea rows="15" className="form-control" value={profile.aboutMe} placeholder="About Me" onChange={(e) => this.handleAboutMeChange(e)} ></textarea>
                </div>

                <div className="form-group">
                    Avatar: <input type="file" className="form-control" onChange={(e) => this.handleAvatarChange(e)} />
                </div>

                <button className="btn btn-success" onClick={(e) => this.handleUpdateProfile(e) } >Update</button>
                <hr/>

                <h2>Password Setting</h2>
                <hr/>
                <div className="form-group">
                    <input type="password" className="form-control" placeholder="Current password" onChange={(e) => this.handlePasswordChange(e) } />
                </div>

                <div className="form-group">
                    <input type="password" className="form-control" placeholder="New password" onChange={(e) => {this.handleNewPasswordChange(e)}} />
                </div>

                <button className="btn btn-success" onClick={(e) => this.handleUpdatePassword(e) }>Update</button>
                <hr/>
                <br/>
            </form>
        );
    }
}