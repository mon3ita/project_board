import React, { Component } from "react";

import { Link } from "react-router-dom";

import { axios_instance, limit_text } from "../../utils";

export default class Users extends Component {

    constructor(props) {
        super(props);

        this.state = {
            users: []
        }
    }

    componentDidMount() {

        const query = `
            query {
                users {
                    username
                        profile {
                      avatar
                      aboutMe
                    }
                }
            }
        `;

        axios_instance.post("/", {
            query: query
        }).then(resp => {
            this.setState({ users: resp.data.data.users });
        }).catch(err => {
                console.log(err);
        });
    }

    render() {
        const users = this.state.users.map((user, idx) => 
            <div className="col-sm-6">
                <div className="card" style={{padding: "10px"}}>
                    <h5 className="card-title"><Link to={`/users/${user.username}`}>{user.username}</Link></h5>
                    {limit_text(user.profile.aboutMe)}
                </div><br/>
            </div>
        );

        return (
            <div className="row">
                {users}
            </div>
        )
    }
}