import React, { Component } from "react";

import { axios_instance } from "../../utils";

export default class SignIn extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            password: "",
            errors: []
        };

        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleUsernameChange(e) {
        this.setState({ username: e.target.value });
    }

    handlePasswordChange(e) {
        this.setState({ password: e.target.value });
    }

    handleLogin(e) {
        const errors = [];

        const user = this.state;

        const mutation = `mutation {
            tokenAuth(username: "${user.username}", password: "${user.password}") {
                token
                payload
            }
        }`;

        axios_instance.post("/", {
            query: mutation
        }).then(response => {
            if(response.data.errors) {
                for(var err of errors) {
                    errors.push(err.message);
                }
                this.setState({ errors });
            } else {
                localStorage.setItem("token", response.data.data.tokenAuth.token);
                localStorage.setItem("username", response.data.data.tokenAuth.payload.username);
                window.location.href = "/";
            }
        }).catch(err => {
            errors.push(err);
        })

        e.preventDefault();
    }

    render() {

        const user = this.state;

        return(
            <form role="form" style={{paddingRight: "50%"}}>
                <h1>Sign In</h1>
                <hr/>
                <div className="form-group row">
                    <input className="form-control" type="text" placeholder="Username" onChange={(e) => this.handleUsernameChange(e) }  value={user.username} />
                </div>

                <div className="form-group row">
                    <input className="form-control" type="password" placeholder="Password" onChange={(e) => this.handlePasswordChange(e)} value={user.password} />
                </div>
                        
                <div className="form-group row">
                    <input type="button" className="btn btn-primary mr-2" value="Sign In" onClick={(e) => this.handleLogin(e) } />
                </div>
            </form>
        )
    }
}