import React, { Component } from "react";

import { axios_instance } from "../../utils";

export default class SignUp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: "",
            email: "",
            password: "",
            errors: []
        };

        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleRegistration = this.handleRegistration.bind(this);
    }

    handleUsernameChange(e) {
        this.setState({ username: e.target.value });
    }

    handleEmailChange(e) {
        this.setState({ email: e.target.value });
    }

    handlePasswordChange(e) {
        this.setState({ password: e.target.value });
    }

    handleRegistration(e) {
        const errors = [];

        const user = this.state;

        if(!user.username) {
            errors.push("Username must be provided.");
        }

        if(!user.email) {
            errors.push("Email must be provided.");
        }

        if(!user.password) {
            errors.push("Password must be provided.");
        }

        if(user.password && user.password.length < 8) {
            errors.push("Password must be at least 8 chars long.");
        }

        if(!errors.length) {
            const mutation = `mutation {
                registration(username: "${user.username}",
                            email: "${user.email}",
                            password: "${String(user.password)}") {
                    user {
                        username
                    }
                }
            }`;

            axios_instance.post("/", {
                    query: mutation
                })
                .then(response => {
                    if(response.data.errors) {
                        for(var err of response.data.errors) {
                            errors.push(err.message);
                        }

                    } else {
                        window.location.href = "/signin";
                    }
                }).catch(err => {
                    errors.push(err);
                    this.setState({ errors });
            });
        }

        this.setState({ errors });
        e.preventDefault();
    }

    render() {
        const errors = this.state.errors.map((err, idx) => 
            <div className="alert alert-danger" role="alert">
                {err}
            </div>
        );

        const user = this.state;

        return(
            <form role="form" style={{paddingRight: "50%"}}>
                <h1>Sign Up</h1>
                <hr/>
                {this.state.errors.length ? <div>{errors}<hr/></div> : ""}
                <div className="form-group row">
                    <input className="form-control" type="text" placeholder="Username" onChange={(e) => this.handleUsernameChange(e) } value={user.username} />
                </div>

                <div className="form-group row">
                    <input className="form-control" type="email" placeholder="Email" onChange={(e) => this.handleEmailChange(e) } value={user.email} />
                </div>

                <div className="form-group row">
                    <input className="form-control" type="password" placeholder="Password"  onChange={(e) => this.handlePasswordChange(e) } value={user.password} />
                </div>
                        
                <div className="form-group row">
                    <input type="button" className="btn btn-primary mr-2" value="Sign Up" onClick={(e) => this.handleRegistration(e) } />
                </div>
            </form>
        )
    }
}