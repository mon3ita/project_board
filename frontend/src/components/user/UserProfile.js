import React, { Component } from "react";

import { Link } from "react-router-dom";

import { axios_instance, getUser, limit_text } from "../../utils";

export default class UserProfile extends Component {

    constructor(props) {
        super(props);

        this.state = {
            user: {},
            profile: {},
            teamProject: [],
            teamTask: [],
            teamStaffProject: [],
            teamStaffTask: []
        };

    }

    componentDidMount() {
        const username = this.props.match.params.username;

        const query = `query {
            userByName(username: "${username}") {
                username
                firstName
                lastName
                email
                profile {
                  aboutMe
                  avatar
                }
                teamprojectSet {
                  project {
                    id
                    name
                    description
                  }
                }
                teamstaffprojectSet {
                    project {
                        id
                        name
                        description
                    }
                }
                teamstafftaskSet {
                    task {
                        id
                        name
                        description 
                        project {
                            id
                        }
                    }
                }
                teamtaskSet {
                  task {
                    id
                    name
                    description
                    project {
                        id
                    }
                  }
                }
              }
        }`;

        axios_instance.post("/", {
            query: query
        }).then(response => {
            this.setState({
                    profile: response.data.data.userByName.profile,
                    user: response.data.data.userByName,
                    teamProject: response.data.data.userByName.teamprojectSet,
                    teamTask: response.data.data.userByName.teamtaskSet,
                    teamStaffTask: response.data.data.userByName.teamstafftaskSet,
                    teamStaffProject: response.data.data.userByName.teamstaffprojectSet
            });
        }).catch(err => {
            console.log(err);
        });
    }

    render() {
        const { user, profile } = this.state;
        const current_user = getUser()[1];

        const projects = this.state.teamProject.length ? this.state.teamProject.map((project, idx) => 
            <div className="card">
                <div className="card-body">
                    <h2 className="card-title"><Link to={`/projects/${project.project.id}`}> {project.project.name}</Link></h2>
                    {limit_text(project.project.description)}
                </div>
            </div>) : <i><h5>Not involved in any project yet.</h5></i>;

        const tasks = this.state.teamTask.length ? this.state.teamTask.map((task, idx) => 
            <div className="card">
                <div className="card-body">
                    <h2 className="card-title"><Link to={`/projects/${task.task.project.id}/tasks/${task.task.id}`} >{task.task.name}</Link></h2>
                    <span style={{wordBreak: "break-all"}}>
                        {limit_text(task.task.description)}
                    </span>
                </div>
            </div>) : <i><h5>Not involved in any task yet.</h5></i>;

        const staff_projects = this.state.teamStaffProject.length ? this.state.teamStaffProject.map((project, idx) => 
            <div className="card">
                <div className="card-body">
                    <h2 className="card-title"><Link to={`/projects/${project.project.id}`} >{project.project.name}</Link></h2>
                    <span style={{wordBreak: "break-all"}}>
                        {limit_text(project.project.description)}
                    </span>
                </div>
            </div>) : <i><h5>Not staff member of any project yet.</h5></i>;

        console.log(this.state.teamStaffTask);
        const staff_tasks = this.state.teamStaffTask.length ? this.state.teamStaffTask.map((task, idx) => 
            <div className="card">
                <div className="card-body">
                    <h2 className="card-title"><Link to={`/projects/${task.task.project.id}/tasks/${task.task.id}`} >{task.name}</Link></h2>
                    <span style={{wordBreak: "break-all"}}>
                        {limit_text(task.task.description)}
                    </span>
                </div>
            </div>) : <i><h5>Not staff member of any task yet.</h5></i>;

        return(
            <div>
                {current_user == user.username ? 
                    <div>
                        <Link className="btn btn-success btn-sm" to={`/users/${current_user}/update`}>Update</Link><hr/>
                    </div>
                    : ""}
                <h1>{user.username}'s profile</h1>
                <hr/>
                <div className="row">
                    <div className="col">
                        <img src={`http://localhost:8000/${profile.avatar}`} style={{"width": "150px", "height": "150px", "objectFit": "cover"}} />
                        <br/>
                        {user.firstName ? <div><hr/>{user.firstName} {user.lastName}</div> : ""}
                        <hr/>
                        {profile.aboutMe}
                    </div>
                    <div className="col">
                        <h4>Member</h4>
                        <hr/>
                        <h3>Projects</h3>
                        <hr/>
                        {projects}
                        <hr/>
                        <h3>Tasks</h3>
                        <hr/>
                        {tasks}
                    </div>
                    <div className="col">
                        <h4>Staff</h4>
                        <hr/>
                        <h3>Projects</h3>
                        <hr/>
                        {staff_projects}
                        <hr/>
                        <h3>Tasks</h3>
                        <hr/>
                        {staff_tasks}
                    </div>
                </div>
                <br/>
            </div>
        );
    }
}