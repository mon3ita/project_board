import React, { Component } from "react";

import { axios_instance, getUser } from "../../utils";

export default class ProjectCreateForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: "",
            description: "",
            category: "",
            categories: [],
            errors: []
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleCategoryChange = this.handleCategoryChange.bind(this);
        this.handleCreateProject = this.handleCreateProject.bind(this);
    }

    componentDidMount() {

        const query = `
            query {
                categories {
                    name
                }
            }
        `;

        const token = getUser()[0];
        const headers = {
            "Authorization": `JWT ${token}`
        };

        axios_instance.post("/", {
            query: query
        }, {
            headers: headers
        }).then(response => {
            this.setState({ categories: response.data.data.categories,
                            category: response.data.data.categories.length ? 
                                response.data.data.categories[0] : "" });
        }).catch(err => {
            console.log(err);
        })
    }

    handleNameChange(e) {
        this.setState({ name: e.target.value });
    }

    handleDescriptionChange(e) {
        this.setState({ description: e.target.value });
    }

    handleCategoryChange(e) {
        this.setState({ category: e.target.value });
    }

    handleCreateProject(e) {
        const errors = [];

        const project = this.state;

        if(!project.name)
            errors.push("Name must be provided.")

        if(!project.description)
            errors.push("Description must be provided.")

        if(!project.category)
            errors.push("Category must be provided.")

        if(!errors.length) {
            const token = getUser()[0];
            const headers = {
                "Authorization": `JWT ${token}`
            };

            const mutate = `
                mutation {
                    projectCreate(name: "${project.name}", description: "${project.description}", category: "${project.category}") {
                        project {
                            id
                        }
                    }
                }
            `;

            const errors = [];
            axios_instance.post("/", {
                query: mutate
            }, {
                headers: headers
            }).then(response => {
                if(response.data.data.errors) {
                    for(let error of response.data.data.errors) {
                        errors.push(error.message);
                    }

                    this.setState({ errors });
                } else
                    window.location.href = `/projects/${response.data.data.projectCreate.project.id}`;
            }).catch(err => {
                errors.push(err);
                this.setState({ errors });
            });
        } else
            this.setState({ errors });
        e.preventDefault();
    }

    render() {
        const project = this.state;

        const errors = this.state.errors.map((err, idx) => 
            <div className="alert alert-danger">
                {err}
            </div>);

        const categories = this.state.categories.map((cat, idx) => 
            <option className="form-control" key={idx}>{cat.name}</option>
        );

        return(
            <form role="form" style={{paddingRight: "50%"}}>
                <h1>Create Project</h1>
                <hr/>
                {this.state.errors ? <div>{errors}<hr/></div> : ""}
                <div className="form-group row">
                    <input className="form-control" type="text" placeholder="Title" onChange={(e) => this.handleNameChange(e) } value={project.name} />
                </div>

                <div className="form-group row">
                    <textarea className="form-control" placeholder="Description" onChange={(e) => this.handleDescriptionChange(e) } value={project.description} ></textarea>
                </div>
                    
                <div className="form-group row">
                    <select className="form-control" onChange={(e) => this.handleCategoryChange(e) }>
                        {categories}
                    </select>
                </div>

                <div className="form-group row">
                    <input type="button" className="btn btn-success mr-2" value="Create" onClick={(e) => this.handleCreateProject(e) } />
                </div>
            </form>
        )
    }
}