import React, { Component } from "react";

import { Link } from "react-router-dom";

import { axios_instance, getUser } from "../../utils";

export default class ProjectUpdateForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            project: {},
            teamProjectID: "",
            team: [],
            teamStaffID: "",
            teamStaff: [],
            categories: [],
            users: [],
            errors: []
        }

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleUpdateProject = this.handleUpdateProject.bind(this);
        this.handleDoneChange = this.handleDoneChange.bind(this);
        this.handleDeadlineChange = this.handleDeadlineChange.bind(this);
        this.handleTeamChange = this.handleTeamChange.bind(this);
        this.handleTeamAdd = this.handleTeamAdd.bind(this);
        this.handleTeamStaffChange = this.handleTeamStaffChange.bind(this);
        this.handleTeamStaffAdd = this.handleTeamStaffAdd.bind(this);
        this.handleTeamUpdate = this.handleTeamUpdate.bind(this);
    }

    componentDidMount() {
        const token = getUser()[0];
        const headers = {
            "Authorization": `JWT ${token}`
        };

        const project_id = this.props.match.params.id;

        const query = `
            query {
                projectById(pk: ${project_id}) {
                    id
                    name
                    description
                    category
                    done
                    deadline
                    teamProject {
                        id
                        team {
                            id
                            username
                        }
                    }
                    teamStaffProject {
                        id
                        team {
                            id
                            username
                        }
                    }
                }
                categories {
                    name
                }
                users {
                    id
                    username
                }
            }
        `;

        axios_instance.post("/", {
            query: query
        }, {
            headers: headers
        })
            .then(response => {
                this.setState({
                    project: response.data.data.projectById,
                    teamProjectID: response.data.data.projectById.teamProject.id,
                    team: response.data.data.projectById.teamProject.team,
                    teamStaffID: response.data.data.projectById.teamStaffProject.team.id,
                    teamStaff: response.data.data.projectById.teamStaffProject.team,
                    categories: response.data.data.categories,
                    users: response.data.data.users,
                });
            })
            .catch(err => {
                console.log(err);
            });
    }

    handleNameChange(e) {
        const project = this.state.project;
        project.name = e.target.value;
        this.setState({ project });
    }

    handleDescriptionChange(e) {
        const project = this.state.project;
        project.description = e.target.value;
        this.setState({ project });
    }

    handleDoneChange(e) {
        const { project } = this.state;
        project.done = project.done ? 1 : 0;
        const current = e.target.value ? 1 : 0;
        project.done = Math.abs(current - project.done);
        this.setState({ project });
    }

    handleDeadlineChange(e) {
        const { project } = this.state;
        project.deadline = new Date(e.target.value);
        this.setState({ project });
    }

    handleCategoryChange(e) {
        const { project } = this.state;
        project.category = e.target.value;
        this.setState({ project });
    }

    handleTeamChange(user) {
        const { team } = this.state;
        let new_team = team.filter(member => member.id != user.id);
        this.setState({ team: new_team });
    }

    handleTeamAdd(user) {
        const { team } = this.state;
        const exists = team.filter(member => member.id != user.id).length < team.length;
        if(!exists) {
            let new_team = team;
            new_team.push(user);
            this.setState({ team: new_team });
        }
    }

    handleTeamStaffAdd(user) {
        const { teamStaff } = this.state;
        const exists = teamStaff.filter(member => member.id != user.id).length < teamStaff.length;
        if(!exists) {
            let new_team = teamStaff;
            new_team.push(user);
            this.setState({ teamStaff: new_team });
        }
    }

    handleTeamStaffChange(user) {
        const { teamStaff } = this.state;
        let new_team = teamStaff.filter(member => member.id != user.id);
        this.setState({ teamStaff: new_team });
    }

    handleTeamUpdate(e) {
        const { project, team, teamStaff } = this.state;

        const project_id = this.props.match.params.id;

        let team_arg = "[";
        for(let member of team) {
            team_arg += `{ id: ${member.id}, username: "${member.username}" },`;
        }
        team_arg += "]";

        let team_staff_arg = "[";
        for(let member of teamStaff) {
            team_staff_arg += `{ id: ${member.id}, username: "${member.username}" },`;
        }
        team_staff_arg += "]";

        const mutation = `mutation {
            teamProjectUpdate(pk: ${this.state.teamProjectID},
                              projectId: ${project_id},
                              users: ${team_arg}) {
                success
            }
            teamStaffProjectUpdate(pk: ${this.state.teamProjectID},
                              projectId: ${project_id},
                              users: ${team_staff_arg}) {
                success
            }
        }`;

        const token = getUser()[0];
        const headers = {
            "Authorization": `JWT ${token}`
        };

        const errors = [];
        axios_instance.post("/", {
            query: mutation
        }, {
            headers: headers
        }).then(response => {
            if(response.data.error) {
                errors.push(response.data.error);
                this.setState({ errors });
            } else if(response.data.errors){
                for(let error of response.data.errors) {
                    errors.push(error.message);
                }
                this.setState({ errors });
            } else {
                console.log("updated");
                window.location.href = `/projects/${project_id}`;
            }
        }).catch(err => console.log(err));

        e.preventDefault();
    }

    handleUpdateProject(e) {
        const errors = [];
        const project = this.state.project;

        if(!project.name)
            errors.push("Name must be provided.");

        if(!project.description)
            errors.push("Description must be provided.");


        if(!errors.length) {
            const token = getUser()[0];
            const headers = {
                "Authorization": `JWT ${token}`
            };

            const project_id = this.props.match.params.id;

            const mutation = `
                mutate {
                    projectUpdate(projectId: ${project.id},
                                    name: ${project.name},
                                    description: ${project.description},
                                    done: ${project.done},
                                    deadline: ${project.deadline}) {
                        id
                        name
                        description
                    }
            }`;

            const errors = [];
            axios_instance.post("/", {
                query: mutation
            }, {
                headers: headers
            }).then(response => {
                if(response.data.data.errors) {
                    for(let error of response.data.data.errors) {
                        errors.push(error.message);
                    }
                    this.setState({ errors });
                } else {
                    window.location.href = `/projects/${project.id}`;
                }
            }).catch(err => {
                errors.push(err);
                this.setState({ errors });
            });
        } else
            this.setState({ errors });

        e.preventDefault();
    }

    render() {
        const { project, team, categories, teamStaff } = this.state;

        const errors = this.state.errors.map((err, idx) => 
            <div className="alert alert-danger">
                {err}
            </div>);

        const members = team.map((member, idx) => 
            <span>{member.username}&nbsp;<Link onClick={() => this.handleTeamChange(member)} className="btn btn-danger btn-sm">X</Link>,&nbsp;</span>
        );

        const staff = teamStaff.map((member, idx) => 
            <span>{member.username}&nbsp;<Link onClick={() => this.handleTeamStaffChange(member)} className="btn btn-danger btn-sm">X</Link>,&nbsp;&nbsp;</span>
        );

        const users = this.state.users.map((user, idx) => 
            <span>{user.username}&nbsp;<Link onClick={() => this.handleTeamAdd(user)} className="btn btn-danger btn-sm">Member</Link>,&nbsp;<Link onClick={() => this.handleTeamStaffAdd(user)} className="btn btn-danger btn-sm">Staff</Link></span>
        );

        const options = categories.map((category, idx) =>
            <option key={idx}>{category.name}</option>
        );

        return(
            <form role="form" style={{paddingRight: "50%"}}>
                <h1>Update Project</h1>
                <hr/>
                {this.state.errors.length ? <div>{errors}<hr/></div> : ""}
                <h3>Project Update</h3>
                <hr/>
                <div className="form-group">
                    <input className="form-control" type="text" placeholder="Title" onChange={(e) => this.handleNameChange(e) } value={project.name} />
                </div>

                <div className="form-group">
                    <textarea className="form-control" placeholder="Description" onChange={(e) => this.handleDescriptionChange(e) } value={project.description} ></textarea>
                </div>
                
                <div className="form-group">
                    <select className="form-control">
                        {options}
                    </select>
                </div>

                <div className="form-group">
                    Done: {project.done ? <input className="form-control" type="checkbox" onChange={(e) => this.handleDoneChange(e) } checked /> : <input className="form-control" type="checkbox" onChange={(e) => this.handleDoneChange(e) } />}
                </div>

                <div className="form-group">
                    <input type="date" className="form-control" onChange={(e) => this.handleDeadlineChange(e)} />
                </div>

                <div className="form-group">
                    <input type="button" className="btn btn-primary mr-2" value="Update" onClick={(e) => this.handleUpdateProject(e) } />
                </div>
                <hr/>
                <h3>Team Project Update</h3>
                <hr/>
                <b>Team members:</b> {members}
                <hr/>
                <b>Team staff: </b> {staff}
                <hr/>
                <b>All users:</b> {users}
                <hr/>
                <input type="button" onClick={(e) => this.handleTeamUpdate(e)} className="btn btn-success" value="Update team" />
                <br/>
                <hr/>
            </form>
        )
    }
}