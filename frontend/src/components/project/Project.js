import React, { Component } from "react";

import { Link } from "react-router-dom";

import Tasks from "../task/Tasks";
import Team from "../team/Team";

import { axios_instance, getUser, getDate } from "../../utils";

export default class Project extends Component {

    constructor(props) {
        super(props);

        this.state = {
            project: {},
            tasks: [],
            team: [],
            teamStaff: [],
        };
    }

    componentDidMount() {
        const project_id = this.props.match.params.id;

        const project = `
            query {
                projectById(pk: ${project_id}) {
                    id
                    name
                    description
                    done
                    category
                    createdAt
                    updatedAt
                    deadline
                    owner {
                        username
                    }
                    teamProject {
                      id
                      team {
                        username
                      }
                    }
                    teamStaffProject {
                        team {
                            id
                            username
                        }
                    }
                    taskSet {
                      id
                      name
                      done
                      description
                      createdAt
                      updatedAt
                      deadline
                      teamTask {
                        team {
                          username
                        }
                    }
                }
            }
        }`;

        axios_instance.post("/", {
            query: project
        }).then(response => {
            this.setState({
                project: response.data.data.projectById,
                team: response.data.data.projectById.teamProject.team,
                teamStaff: response.data.data.projectById.teamStaffProject.team,
                tasks: response.data.data.projectById.taskSet
            });
        }).catch(err => {
            console.log(err);
        });
    }

    render() {
        const user = getUser()[1];

        const { project, team, teamStaff } = this.state;

        const members = team.map((member, idx) => 
            <span><Link to={`/users/${member.username}`}>{member.username}</Link>,&nbsp;</span>
        );

        const staff = teamStaff.map((member, idx) => 
            <span><Link to={`/users/${member.username}`} style={{color: "green"}}>{member.username}</Link>,&nbsp;</span>
        );

        const tasks = <Tasks tasks={this.state.tasks} project_id={project.id} />

        return(
            <div>
                {user != undefined ? <div>
                    <button className="btn btn-success btn-sm">Join</button>&nbsp;
                    <Link to={`/projects/${project.id}/update`} className="btn btn-primary btn-sm">Update</Link>&nbsp;
                    <Link to={`/projects/${project.id}/create`} className="btn btn-primary btn-sm">Add Task</Link>
                <hr/>
                </div> : ""}

                <h1>{project.name}</h1>
                <hr/>
                Owner: {project.owner ? project.owner.username : "" } | Category: {project.category} | Deadline: {getDate(new Date(project.deadline))}
                <hr/>
                <div>{project.description}</div>
                <hr />
                <b>Team:</b> {members}
                <hr/>
                <b>Staff:</b> {staff}
                <hr/>
                {tasks}
            </div>
        )
    }
}