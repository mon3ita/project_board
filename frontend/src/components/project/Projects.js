import React, { Component } from "react";

import { Link } from "react-router-dom";

import { axios_instance, limit_text, getDate } from "../../utils";

export default class Projects extends Component {

    constructor(props) {
        super(props);

        this.state = {
            projects: []
        }
    }

    componentDidMount() {
        const query = `query {
                projects {
                    id
                    name
                    category
                    description
                    category
                    deadline
                    done
                    createdAt
                    updatedAt
                    owner {
                        username
                    }
                }
            }`;

        axios_instance.post('/', {
                query: query
            })
            .then(resp => {
                this.setState({projects: resp.data.data.projects});
            }).catch(err => {
                console.log(err);
            })
    }

    render() {
        const projects = this.state.projects.map((project, idx) => 
            <div className="col-sm-6">
                <div className="card" style={{padding: "10px"}}>
                    <h5 className="card-title">
                        <Link to={`/projects/${project.id}`}>
                            {project.name} {project.done ? <img src="https://findicons.com/files/icons/573/must_have/48/check.png" width="10" /> :
                            <img src="https://findicons.com/files/icons/2015/24x24_free_application/24/delete.png" width="10" />}
                        </Link>
                    </h5>
                    <p className="card-text">{limit_text(project.description)}</p>
                    <footer>
                        <i>Owner: <Link to={`/users/${project.owner.username}`}>
                            {project.owner.username}
                        </Link>
                        &nbsp;&nbsp;|&nbsp;&nbsp;
                        Category: {project.category}
                        &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;
                        Deadline: { getDate(new Date(project.deadline)) }</i>
                    </footer>
                </div><br/>
            </div>
        );

       return(
            <div>
                <Link to="/projects/create" className="btn btn-success btn-sm">Create</Link>
                <hr/>
                <div className="row">
                    {projects}
                </div>
            </div>
        )
    }
}