import React, { Component } from "react";

import { Link } from "react-router-dom";

import { axios_instance } from "../../utils";

import Team from "../team/Team";

import { limit_text } from "../../utils";

export default class Tasks extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tasks: []
       }
    }


    componentDidMount() {
        this.setState({ tasks: this.props.tasks });
    }

    render() {
        const project_id = this.props.project_id;

        const tasks = this.state.tasks.map((task, idx) => 
            <div className="col-lg-4 mb-4">
                <div className="card border-primary h-100">
                    <div className="card-body d-flex flex-column align-items-start">
                    <h4 className="card-title text-primary"><Link to={`/projects/${project_id}/tasks/${task.id}`}>{task.name}</Link></h4>
                        <p class="card-text" style={{wordBreak: "break-all"}}>{limit_text(task.description)}</p>
                    </div>
                </div>
            </div>
        );

        return(
            <div className="row">
                {tasks}
            </div>
        )
    }
}