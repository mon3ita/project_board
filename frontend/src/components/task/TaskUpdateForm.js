import React, { Component } from "react";

import { Link } from "react-router-dom";

import { axios_instance, getUser } from "../../utils";


export default class TaskUpdateForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            task: {},
            team: [],
            teamID: "",
            teamStaff: [],
            teamStaffID: "",
            errors: [],
            users: []
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleTaskUpdate = this.handleTaskUpdate.bind(this);
        this.handleDeadlineChange = this.handleDeadlineChange.bind(this);
        this.handleDoneChange = this.handleDoneChange.bind(this);
        this.handleTeamChange = this.handleTeamChange.bind(this);
        this.handleAddToTeam = this.handleAddToTeam.bind(this);
        this.handleTeamStaffChange = this.handleTeamStaffChange.bind(this);
        this.handleAddToTeamStaff = this.handleAddToTeamStaff.bind(this);
        this.handleTeamUpdate = this.handleTeamUpdate.bind(this);
    }

    componentDidMount() {
        const task_id = this.props.match.params.task_id;
        const project_id = this.props.match.params.id;

        const query = `
            query {
                taskById(pk: ${task_id}) {
                    id
                    name
                    description
                    deadline
                    done
                    teamTask {
                        id
                        team {
                            id
                            username
                        }
                    }
                    teamStaffTask {
                        id
                        team {
                            id
                            username
                        }
                    }
                }
                users {
                    id
                    username
                }
            }
        `;

        const token = getUser()[0];
        const headers = {
            "Authorization": `JWT ${token}`
        };

        axios_instance.post("/", {
            query: query
        }, {
            headers: headers
        }).then(response => {
            this.setState({
                task: response.data.data.taskById,
                team: response.data.data.taskById.teamTask.team,
                teamID: response.data.data.taskById.teamTask.id,
                teamStaff: response.data.data.taskById.teamStaffTask.team,
                teamStaffID: response.data.data.taskById.teamStaffTask.id,
                users: response.data.data.users
            });
        }).catch(err => {
            console.log(err);
        });
    }

    handleNameChange(e) {
        const { task } = this.state;
        task.name = e.target.value;
        this.setState({ task });
    }

    handleDescriptionChange(e) {
        const { task } = this.state;
        task.description = e.target.value;
        this.setState({ task });
    }

    handleDeadlineChange(e) {
        const { task } = this.state;
        task.deadline = new Date(e.target.value);
        this.setState({ task });
    }

    handleTeamChange(id) {
        const { team } = this.state;
        const new_team = team.filter((task, idx) => idx != id);
        this.setState({ team: new_team });
    }

    handleAddToTeam(user) {
        const { team } = this.state;
        const exists = team.filter(member => member.username != user.username).length < team.length;
        if(! exists) {
            team.push(user);
            this.setState({ team });
        }
    }


    handleTeamStaffChange(id) {
        const { teamStaff } = this.state;
        const new_team = teamStaff.filter((task, idx) => idx != id);
        this.setState({ teamStaff: new_team });
    }

    handleAddToTeamStaff(user) {
        const { teamStaff } = this.state;
        const exists = teamStaff.filter(member => member.username != user.username).length < teamStaff.length;
        if(! exists) {
            teamStaff.push(user);
            this.setState({ teamStaff });
        }
    }

    handleDoneChange(e) {
        const { task } = this.state;
        task.done = task.done ? 1 : 0;
        const current = e.target.value ? 1 : 0;
        task.done = Math.abs(current - task.done);
        this.setState({ task });
    }

    handleTaskUpdate(e) {
        const errors = [];
        const { task } = this.state;

        const project_id = this.props.match.params.id;

        if(!errors.length) {
            const token = getUser()[0];

            const headers = {
                "Authorization": `JWT ${token}`
            };

            let deadline = new Date(task.deadline);
            deadline = `${deadline.getFullYear()}-${deadline.getMonth()}-${deadline.getDate()}`;
           
            const mutate = `
                mutation updateTask($pk: ID!,
                                    $name: String!,
                                    $deadline: String!,
                                    $done: Boolean!,
                                    $description: String!) {
                    taskUpdate(pk: $pk,
                                name: $name, 
                                deadline:$deadline,
                                done: $done,
                                description: $description) {
                        task {
                            id
                        }
                    }
                }
            `;

            const variables = {
                pk: task.id,
                name: task.name,
                done: task.done,
                deadline: task.deadline,
                description: task.description
            };

            const errors = [];
            axios_instance.post("/", {
                query: mutate,
                variables: variables
            }, {
                headers: headers
            }).then(response => {
                if(response.data.errors) {
                    for(let error of response.data.errors) {
                        errors.push(error.message);
                    }
                    this.setState({ errors });
                } else {
                    window.location.href = `/projects/${project_id}/tasks/${task.id}`;
                }
            }).catch(err => {
                errors.push(err);
                this.setState({ errors });
            });
        } else 
            this.setState({ errors });

        e.preventDefault();
    }

    handleTeamUpdate(e) {

        const { task, team, teamStaff, teamID, teamStaffID } = this.state;
        const project_id = this.props.match.params.id;

        let team_arg = "[";
        for(let member of team) {
            team_arg += `{ id: ${member.id}, username: "${member.username}"},`;
        }
        team_arg += "]";

        let team_staff_arg = "[";
        for(let member of teamStaff) {
            team_staff_arg += `{ id: ${member.id}, username: "${member.username}"},`;
        }
        team_staff_arg += "]";

        const mutation = `
            mutation {
                teamTaskUpdate(pk: ${teamID}, users: ${team_arg}) {
                    success
                    error
                }
                teamStaffTaskUpdate(pk: ${teamStaffID}, users: ${team_staff_arg}) {
                    success
                    error
                }
            }
        `;

        const token = getUser()[0];
        const headers = {
            "Authorization": `JWT ${token}`
        };

        const errors = [];
        axios_instance.post("/", {
            query: mutation
        }, {
            headers: headers
        }).then(response => {
            if(response.data.error) {
                errors.push(response.data.error);
                this.setState({ errors });
            } else if(response.data.errors){
                for(let error of response.data.errors) {
                    errors.push(error.message);
                }
                this.setState({ errors });
            } else {
                window.location.href = `/projects/${project_id}/tasks/${task.id}`;
            }
        }).catch(err => console.log(err));

        e.preventDefault();
    }

    render() {
        const { task, team } = this.state;
        const errors = this.state.errors.map((err, idx) =>
            <div className="alert alert-danger">
                {err}
            </div>
        );

        const members = team.map((member, idx) => 
            <span>
                {member.username}&nbsp;<Link onClick={() => this.handleTeamChange(idx)} className="btn btn-danger btn-sm">X</Link>&nbsp;,&nbsp;&nbsp;
            </span>
        );

        const all_users = this.state.users.map((user, idx) =>
            <span>
                {user.username}&nbsp;<Link onClick={() => this.handleAddToTeam(user)} className="btn btn-danger btn-sm">Member</Link>&nbsp;<Link onClick={() => this.handleAddToTeamStaff(user)} className="btn btn-danger btn-sm">Staff</Link>&nbsp;,&nbsp;
            </span> 
        );

        const staff = this.state.teamStaff.map((member, idx) => 
            <span>
                {member.username}&nbsp;<Link onClick={() => this.handleTeamStaffChange(idx)} className="btn btn-danger btn-sm">X</Link>&nbsp;,&nbsp;&nbsp;
            </span>
        );

        const deadline = new Date(task.deadline);
        return(
            <form role="form" style={{paddingRight: "50%"}}>
                <h1>Update Task</h1>
                <hr/>
                {this.state.errors ? <div>{errors}<hr/></div>: ""}
                <h3>Task Properties</h3>
                <hr/>
                <div className="form-group">
                    <input className="form-control" type="text" value={task.name} placeholder="Title" onChange={(e) => this.handleTitleUpdate(e) } />
                </div>

                <div className="form-group">
                    <textarea className="form-control" value={task.description} placeholder="Description" onChange={(e) => this.handleDescriptionChange(e) } ></textarea>
                </div>

                <div className="form-group">
                    Done: {task.done ? <input className="form-control" type="checkbox" onChange={(e) => this.handleDoneChange(e) } checked /> : <input className="form-control" type="checkbox" onChange={(e) => this.handleDoneChange(e) } />}
                </div>

                <div className="form-group">
                    <input className="form-control" type="date" value={`${deadline.getFullYear()}-${deadline.getMonth()}-${deadline.getDate()}`} onChange={(e) => this.handleDeadlineChange(e) } />
                </div>  

                <div className="form-group">
                    <input type="button" className="btn btn-primary mr-2" value="Update" onClick={(e) => this.handleTaskUpdate(e) } />
                </div>

                <hr/>
                <h3>Team Properties</h3>
                <hr/>
                <div className="form-group">
                    <b>Team members: </b> {members}
                </div>
                <hr/>
                <div className="form-group">
                    <b>Staff: </b> {staff}
                </div>
                <hr/>
                <div className="form-group">
                    <b>Users: </b> {all_users}
                </div>
                <hr/>

                <div className="form-group">
                    <input type="button" className="btn btn-primary mr-2" value="Update" onClick={(e) => this.handleTeamUpdate(e) } />
                </div>
            </form>
        )
    }
}