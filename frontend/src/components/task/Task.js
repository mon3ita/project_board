import React, { Component } from "react";

import { Link } from "react-router-dom";

import { axios_instance, getUser, limit_text, getDate } from "../../utils";

import Team from "../team/Team";

export default class Task extends Component {

    constructor(props) {
        super(props);

        this.state = {
            task: {},
            team: [],
            teamStaff: []
        };
    }

    componentDidMount() {
        const task_id = this.props.match.params.task_id;

        const project = `
            query {
                taskById(pk: ${task_id}) {
                    id
                    name
                    description
                    done
                    createdAt
                    updatedAt
                    deadline
                    owner {
                        username
                    }
                    project {
                        id
                        name
                    }
                    teamTask {
                      team {
                        username
                      }
                    }
                    teamStaffTask {
                        team {
                            username
                        }
                    }
            }
        }`;

        axios_instance.post("/", {
            query: project
        }).then(response => {
            this.setState({
                task: response.data.data.taskById,
                team: response.data.data.taskById.teamTask.team,
                teamStaff: response.data.data.taskById.teamStaffTask.team,
            });
        }).catch(err => {
            console.log(err);
        });
    }

    render() {
        const { task, team } = this.state;
        const project_id = this.props.match.params.id;
        const current_user = getUser()[1];

        const members = this.state.team.map((member, idx) => 
            <span><Link to={`/users/${member.username}`}>{member.username}</Link>,&nbsp;</span>
        );

        const staff = this.state.teamStaff.map((member, idx) => 
            <span><Link to={`/users/${member.username}`} style={{color: "green"}}>{member.username}</Link>,&nbsp;</span>
        );

        return(
            <div>
                <Link to={`/projects/${project_id}`} className="btn btn-primary btn-sm">Back</Link>&nbsp;
                {task.owner && task.owner.username == current_user ? <Link className="btn btn-primary btn-sm" to={`/projects/${project_id}/tasks/${task.id}/update`}>Update</Link> : ""}<hr/>
                <h2>{task.name}</h2>
                <hr/>
                Project: {task.project ? task.project.name : ""} | Deadline: {getDate(new Date(task.deadline))}
                <hr/>
                Status: <b><i>{task.done ? "Done" : "In progress"}</i></b>
                <hr/>
                <span style={{wordBreak: "break-all"}}>{task.description}</span>
                <hr/>
                <b>Team:</b> {members}
                <hr/>
                <b>Staff:</b> {staff}
                <hr/>
            </div>
        )
    }
}