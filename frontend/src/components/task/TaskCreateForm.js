import React, { Component } from "react";

import { Link } from "react-router-dom";

import { axios_instance, getUser } from "../../utils";

export default class TaskCreateForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: "",
            description: "",
            errors: []
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleTaskCreate = this.handleTaskCreate.bind(this);
    }

    handleNameChange(e) {
        this.setState({ name: e.target.value });
    }

    handleDescriptionChange(e) {
        this.setState({ description: e.target.value });
    }

    handleTaskCreate(e) {
        const errors = [];
        const task = this.state;

        const project_id = this.props.match.params.id;

        if(!errors.length) {
            const token = getUser()[0];

            const headers = {
                "Authorization": `JWT ${token}`
            };

            const mutate = `
                mutation createTask($projectId: ID!, $name: String!, $description: String!) {
                    taskCreate(projectId: $projectId, name: $name, description: $description) {
                        task {
                            id
                        }
                    }
                }
            `;

            const variables = {
                projectId: project_id,
                name: task.name,
                description: task.description
            };

            const errors = [];

            axios_instance.post("/", {
                query: mutate,
                variables: variables
            }, {
                headers: headers
            }).then(response => {
                if(response.data.data.errors) {
                    for(let error of response.data.data.errors) {
                        errors.push(error.message);
                    }
                    this.setState({ errors });
                } else
                    window.location.href = `/projects/${project_id}/tasks/${response.data.data.taskCreate.task.id}`
            }).catch(err => {
                errors.push(err);
                this.setState({ errors });
            });
        } else 
            this.setState({ errors });

        e.preventDefault();
    }

    render() {
        const errors = this.state.errors.map((err, idx) =>
            <div className="alert alert-danger">
                {err}
            </div>);
        
        const task = this.state;

        return(
            <form role="form" style={{paddingRight: "50%"}}>
                <h1>Create Task</h1>
                <hr/>
                {this.state.errors.length ? <div>{errors}<hr/></div> : ""}
                <div className="form-group row">
                    <input className="form-control" type="text" placeholder="Title" onChange={(e) => this.handleNameChange(e)} value={task.name} />
                </div>

                <div className="form-group row">
                    <textarea className="form-control" placeholder="Description" onChange={(e) => this.handleDescriptionChange(e) } value={ task.description } ></textarea>
                </div>
                        
                <div className="form-group row">
                    <button className="btn btn-primary mr-2" onClick={(e) => this.handleTaskCreate(e) }>Create</button>
                </div>
            </form>
        )
    }
}