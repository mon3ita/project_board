import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { getUser, logOut } from "../../utils";

export default class Header extends Component {

    render() {
        const username = getUser()[1] || "Guest";

        return(
        <nav className="navbar fixed-top navbar-expand-lg navbar-dark bg-primary" id="navbar1">
                <div className="container">
                    <a className="navbar-brand mr-1 mb-1 mt-0" href="/">ProjectBoard</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="navbar-collapse collapse justify-content-center" id="collapsingNavbar">
                        <ul className="navbar-nav">
                        </ul>
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <a className="nav-link" href="/projects">
                                  Projects
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="/users">Users</a>
                            </li>
                            
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle" href="/#" id="navbarDd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Hello, {username}
                                </a>
                                {username != "Guest" ? 
                                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDd">
                                        <Link className="dropdown-item px-2" to={`/users/${username}`}>My Profile</Link>
                                        <Link className="dropdown-item px-2" to={`/users/${username}/update`}>Update Profile</Link>
                                        <Link className="dropdown-item px-2" onClick={logOut}>Log Out</Link>
                                    </div> : <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDd">
                                                <a className="dropdown-item px-2" href="/signin">Sign In</a>
                                                <a className="dropdown-item px-2" href="/signup">Sign Up</a>
                                            </div> }
                            </li>
                        </ul>
                    </div>
                </div>
        </nav> 
        )
    }
}