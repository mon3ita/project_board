import React, { Component } from 'react';

import { Switch, Route } from "react-router-dom";

import Home from "../home/Home";

import Project from "../project/Project";
import ProjectCreateForm from "../project/ProjectCreateForm";
import ProjectUpdateForm from "../project/ProjectUpdateForm";
import Projects from "../project/Projects";

import Task from "../task/Task";
import TaskCreateForm from "../task/TaskCreateForm";
import TaskUpdateForm from "../task/TaskUpdateForm";
import Tasks from "../task/Tasks";

import Team from "../team/Team";

import SignIn from "../user/SignIn";
import SignUp from "../user/SignUp";
import UserProfile from "../user/UserProfile";
import UserProfileUpdate from "../user/UserProfileUpdate";
import Users from "../user/Users";

export default class Body extends Component {

    render() {
        return(
            <main style={{marginTop: "100px"}}>
                <section className="container">
                    <Switch>
                        <Route exact path="/" component={Home} />

                        <Route exact path="/projects" component={Projects} />
                        <Route exact path="/projects/create" component={ProjectCreateForm} />
                        <Route exact path="/projects/:id" component={Project} />
                        <Route exact path="/projects/:id/update" component={ProjectUpdateForm} />

                        <Route exact path="/projects/:id/tasks" component={Tasks} />
                        <Route exact path="/projects/:id/create" component={TaskCreateForm} />
                        <Route exact path="/projects/:id/tasks/:task_id" component={Task} />
                        <Route exact path="/projects/:id/tasks/:task_id/update" component={TaskUpdateForm} />

                        <Route exact path="/projects/:id/team" component={Team} />

                        <Route exact path="/signin" component={SignIn} />
                        <Route exact path="/signup" component={SignUp} />
                        <Route exact path="/users" component={Users} />
                        <Route exact path="/users/:username" component={UserProfile} />
                        <Route exact path="/users/:username/update" component={UserProfileUpdate} />
                    </Switch>
                </section>
            </main>
        )
    }
}