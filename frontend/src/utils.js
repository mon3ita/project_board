import axios from 'axios';

const axios_instance = axios.create({
    baseURL: 'http://127.0.0.1:8000/app',
    headers: {
        'Content-Type': 'application/json'
    },
    xsrfCookieName: 'csrftoken',
    xsrfHeaderName: 'X-CSRFToken'
});

function limit_text(text, limit=100) {
    return text ? text.slice(0, limit) : "";
}

function getUser() {
    const token = localStorage.getItem("token");
    const username = localStorage.getItem("username");

    return [token, username];
}

function logOut() {
    //fix
    localStorage.removeItem("token");
    localStorage.removeItem("username");
    localStorage.removeItem("id");
    window.location.href = "/signin";
}

function getDate(date) {
    return `${date.getDate()} / ${date.getMonth()} / ${date.getFullYear()}`;
}

export { axios_instance, limit_text, getUser, logOut, getDate };