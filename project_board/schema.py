import project.schema
import user_profile.schema
import team.schema

import graphene
import graphql_jwt

from graphene_django.debug import DjangoDebug


class Query(
    project.schema.Query,
    user_profile.schema.Query,
    team.schema.Query,
    graphene.ObjectType,
):
    debug = graphene.Field(DjangoDebug, name="_debug")


class Mutation(
    user_profile.schema.Mutation, project.schema.Mutation, team.schema.Mutation,
):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()

    debug = graphene.Field(DjangoDebug, name="_debug")


schema = graphene.Schema(query=Query, mutation=Mutation)
