from django.contrib import admin

from .models import TeamProject, TeamTask, TeamStaffProject, TeamStaffTask

admin.site.register(TeamTask)
admin.site.register(TeamProject)
admin.site.register(TeamStaffTask)
admin.site.register(TeamStaffProject)
