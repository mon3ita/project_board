from .models import TeamProject, TeamTask, TeamStaffProject, TeamStaffTask

import graphene
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType

from graphql_jwt.decorators import login_required

from user_profile.schema import UserType, UserInputType


class TeamProjectType(DjangoObjectType):
    class Meta:
        model = TeamProject


class TeamTaskType(DjangoObjectType):
    class Meta:
        model = TeamTask


class TeamStaffProjectType(DjangoObjectType):
    class Meta:
        model = TeamStaffProject


class TeamStaffTaskType(DjangoObjectType):
    class Meta:
        model = TeamStaffTask


class TeamProjectUpdate(graphene.Mutation):
    class Arguments:
        pk = graphene.ID()
        project_id = graphene.ID()
        users = graphene.List(UserInputType)

    success = graphene.String()
    error = graphene.String()

    @login_required
    def mutate(self, info, pk, project_id, users, **kwargs):
        team_project = TeamProject.objects.filter(pk=pk, project_id=project_id).get()

        team_project.team.clear()
        for user in users:
            team_project.team.add(user["id"])
        return TeamProjectUpdate(success="Team was updated successfully.")


class TeamTaskUpdate(graphene.Mutation):
    class Arguments:
        pk = graphene.ID()
        users = graphene.List(UserInputType)

    success = graphene.String()
    error = graphene.String()

    @login_required
    def mutate(self, info, pk, users):
        team_task = TeamTask.objects.get(pk=pk)

        team_task.team.clear()
        for user in users:
            team_task.team.add(user["id"])
        return TeamTaskUpdate(success="Team was updated successfully.")


class TeamStaffProjectUpdate(graphene.Mutation):
    class Arguments:
        pk = graphene.ID()
        project_id = graphene.ID()
        users = graphene.List(UserInputType)

    success = graphene.String()

    @login_required
    def mutate(self, info, pk, project_id, users):
        print(dir(TeamStaffProject))
        team_staff_project = TeamStaffProject.objects.filter(
            pk=pk, project_id=project_id
        ).get()

        team_staff_project.team.clear()
        for user in users:
            team_staff_project.team.add(user["id"])
        return TeamStaffProjectUpdate(success="Team was updated successfully.")


class TeamStaffTaskUpdate(graphene.Mutation):
    class Arguments:
        pk = graphene.ID()
        users = graphene.List(UserInputType)

    success = graphene.String()
    error = graphene.String()

    @login_required
    def mutate(self, info, pk, users):
        team_staff_task = TeamStaffTask.objects.get(pk=pk)

        team_staff_task.team.clear()
        for user in users:
            team_staff_task.team.add(user["id"])
        return TeamStaffTaskUpdate(success="Team was updated successfully.")


class Query(object):
    team_project = graphene.Node.Field(TeamProjectType)
    team_task = graphene.Node.Field(TeamTaskType)
    team_staff_project = graphene.Node.Field()
    team_staff_task = graphene.Node.Field()


class Mutation(graphene.ObjectType):
    team_project_update = TeamProjectUpdate.Field()
    team_task_update = TeamTaskUpdate.Field()
    team_staff_project_update = TeamStaffProjectUpdate.Field()
    team_staff_task_update = TeamStaffTaskUpdate.Field()
