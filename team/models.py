from django.db import models

from django.contrib.auth.models import User
from project.models import Project, Task


class TeamProject(models.Model):
    team = models.ManyToManyField(User)
    project = models.OneToOneField(
        Project,
        verbose_name="project",
        related_name="team_project",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        team = ",".join(user.username for user in self.team.all())
        return f"{self.project.name}'s team / {team}"


class TeamTask(models.Model):
    team = models.ManyToManyField(User)
    task = models.OneToOneField(
        Task, verbose_name="task", related_name="team_task", on_delete=models.CASCADE
    )

    def __str__(self):
        team = ",".join(user.username for user in self.team.all())
        return f"{self.task.name}'s team / {team}"


class TeamStaffProject(models.Model):
    team = models.ManyToManyField(User)
    project = models.OneToOneField(
        Project,
        verbose_name="project",
        related_name="team_staff_project",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        team = ", ".join(user.username for user in self.team.all())
        return f"{self.project.name}'s Staff Team [{team}]"


class TeamStaffTask(models.Model):
    team = models.ManyToManyField(User)
    task = models.OneToOneField(
        Task,
        verbose_name="task",
        related_name="team_staff_task",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        team = ", ".join(user.username for user in self.team.all())
        return f"{self.task.name}'s Staff Team [{team}]"
