from django.db.models.signals import post_save
from django.dispatch import receiver

from django.utils.translation import gettext_lazy as _

from django.contrib.auth.models import User
from project.models import Project, Task

from .models import TeamProject, TeamTask, TeamStaffProject, TeamStaffTask


@receiver(post_save, sender=Project)
def create_team_project(sender, instance, created, **kwargs):
    if created:
        TeamProject.objects.create(project=instance)
        TeamStaffProject.objects.create(project=instance)


@receiver(post_save, sender=Project)
def save_team_project(sender, instance, **kwargs):
    instance.team_project.save()
    instance.team_staff_project.save()


@receiver(post_save, sender=Task)
def create_team_task(sender, instance, created, **kwargs):
    if created:
        TeamTask.objects.create(task=instance)
        TeamStaffTask.objects.create(task=instance)


@receiver(post_save, sender=Task)
def save_team_task(sender, instance, **kwargs):
    instance.team_task.save()
    instance.team_staff_task.save()
